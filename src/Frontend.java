import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import julian.utils.gui.EditorCodigo;
import julian.utils.gui.EditorCodigo.StatusFlag;
import julian.utils.gui.EditorGUI;
import java.awt.Toolkit;

public class Frontend {

	private JFrame frame;
	private EditorGUI ide;
	public static EditorCodigo DB;
	private JMenuBar menuBar;
	private JPanel container;
	private JScrollPane scrollPane;
	public static JTextArea stdout;
	private JLabel lblConsola;
	private JPanel gui;
	private static final String formato_fecha = "dd/MM/yyyy HH:mm:ss";
	private Interprete currentInterprete;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		if (args.length > 0)
			readArgs(args[0]);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					try {
						UIManager
								.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					} catch (Exception e) {// Manejo de excepción...
						try {
							UIManager
									.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
						} catch (Exception e1) {// Manejo de excepción...
							e.printStackTrace();
						}
					}
					setUIFont(new javax.swing.plaf.FontUIResource("Consolas",
							Font.PLAIN, 12));
					Frontend window = new Frontend();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void setUIFont(javax.swing.plaf.FontUIResource f) {
		@SuppressWarnings("rawtypes")
		java.util.Enumeration keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value != null
					&& value instanceof javax.swing.plaf.FontUIResource)
				UIManager.put(key, f);
		}
	}

	/**
	 * Create the application.
	 */
	public Frontend() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setIconImage(Toolkit.getDefaultToolkit().getImage(
				Frontend.class.getResource("/resources/icon.png")));
		this.frame.setTitle("Interprete 3D");
		this.frame.setBounds(100, 100, 637, 443);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.ide = new EditorGUI();
		DB = new EditorCodigo();
		DB.editable(false);
		this.ide.agregarComponente("MySQL.db", DB, -1, EditorGUI.SALIDAS, true);

		this.menuBar = new JMenuBar();
		this.frame.setJMenuBar(this.menuBar);

		this.container = new JPanel();
		this.frame.getContentPane().add(this.container, BorderLayout.CENTER);

		this.scrollPane = new JScrollPane();

		this.lblConsola = new JLabel("Consola");
		this.lblConsola.setFont(new Font("Consolas", Font.PLAIN, 11));

		this.gui = new JPanel();
		GroupLayout gl_container = new GroupLayout(this.container);
		gl_container
				.setHorizontalGroup(gl_container
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								gl_container
										.createSequentialGroup()
										.addGroup(
												gl_container
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_container
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				this.gui,
																				GroupLayout.DEFAULT_SIZE,
																				601,
																				Short.MAX_VALUE))
														.addGroup(
																gl_container
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				this.lblConsola))
														.addGroup(
																gl_container
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				this.scrollPane,
																				GroupLayout.DEFAULT_SIZE,
																				601,
																				Short.MAX_VALUE)))
										.addContainerGap()));
		gl_container.setVerticalGroup(gl_container.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_container
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(this.gui, GroupLayout.DEFAULT_SIZE, 209,
								Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(this.lblConsola)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(this.scrollPane,
								GroupLayout.PREFERRED_SIZE, 126,
								GroupLayout.PREFERRED_SIZE).addContainerGap()));

		stdout = new JTextArea();
		stdout.setEditable(false);
		this.scrollPane.setViewportView(stdout);
		this.container.setLayout(gl_container);
		this.gui.setLayout(new BorderLayout(0, 0));
		this.gui.add(this.ide);
		cargarMenus();
	}

	private void cargarMenus() {
		String[] items_archivo = new String[] { "Abrir un archivo...",
				"Actualizar archivo...", "Salir" };
		ActionListener[] listeners_archivo = new ActionListener[] {
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						EditorCodigo codigo = new EditorCodigo();
						File f = abrirArchivo(
								"Archivos de codigo de tres direcciones.",
								"3d", codigo);
						if (f != null) {
							codigo.cargarArchivo(f);
							codigo.editable(false);
							ide.agregarComponente(f.getName(), codigo, -1,
									EditorGUI.ENTRADAS, false);
						}
					}
				}, new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						EditorCodigo editor = (EditorCodigo) (EditorCodigo) ide
								.componenteActual(EditorGUI.ENTRADAS);
						if (editor != null)
							editor.refrescarArchivo();
					}
				}, new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						System.exit(0);
					}
				} };
		agregarMenu("Archivo", items_archivo, listeners_archivo);

		String[] items_ejecutar = new String[] { "Ejecutar", "Detener" };

		ActionListener[] listeners_ejecutar = new ActionListener[] {
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						final EditorCodigo selected = (EditorCodigo) ide
								.componenteActual(EditorGUI.ENTRADAS);
						if (selected != null && currentInterprete == null) {
							new Thread(new Runnable() {
								@Override
								public void run() {
									try {
										// TODO Auto-generated method stub
										long start = System.currentTimeMillis();
										currentInterprete = new Interprete(
												selected.getF());
										currentInterprete.run();
										long end = System.currentTimeMillis();
										selected.actualizarEstado(
												String.format(
														"Script ejecutado en %s.%s segundos.",
														(end - start) / 1000,
														(end - start) % 1000),
												StatusFlag.OK);
										DB.actualizarEstado(String.format(
												"%s: Archivo %s generado.",
												now(), Properties.__path__),
												StatusFlag.OK);
									} catch (Exception e2) {
										e2.printStackTrace();
										selected.actualizarEstado(
												"Ejecucion fallida :(",
												StatusFlag.ERROR);
									}
									currentInterprete = null;
								}
							}).start();
						}
					}
				}, new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (currentInterprete != null)
							currentInterprete.stop = true;
					}
				} };
		agregarMenu("Ejecutar", items_ejecutar, listeners_ejecutar);
	}

	/**
	 * Agrega un menu en la barra de herramientas.
	 * 
	 * @param nombre
	 *            El nombre del menu.
	 * @param items
	 *            La lista de items del menu.
	 * @param listeners
	 *            Las acciones que ejecutará cada item.
	 */
	public void agregarMenu(String nombre, String[] items,
			ActionListener[] listeners) {
		if (items.length != listeners.length) {
			System.err
					.println("El tamaño de la lista de items y listeners debe ser el mismo");
			return;
		}
		JMenu jm = new JMenu(nombre);
		int index = 0;
		for (String s : items) {
			JMenuItem jmi = new JMenuItem(s);
			jmi.addActionListener(listeners[index]);
			++index;
			jm.add(jmi);
		}

		this.menuBar.add(jm);
	}

	/**
	 * Abre un archivo en un editor.
	 * 
	 * @param desc_filtro
	 *            La descripcion del filtro
	 * @param filtro
	 *            La extension del filtro
	 * @param editor
	 *            El editor donde se va a cargar
	 * @return El archivo cargado
	 */
	public File abrirArchivo(String desc_filtro, String filtro,
			EditorCodigo editor) {
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				desc_filtro, filtro);
		final JFileChooser fc = new JFileChooser();
		fc.setFileFilter(filter);
		int returnVal = fc.showOpenDialog(this.frame);
		if (returnVal == JFileChooser.APPROVE_OPTION)
			return fc.getSelectedFile();
		else if (returnVal == JFileChooser.CANCEL_OPTION)
			return null;
		else if (returnVal == JFileChooser.ERROR_OPTION)
			editor.actualizarEstado("Error al abrir el archivo.",
					StatusFlag.ERROR);
		else
			editor.actualizarEstado("Error desconocido.", StatusFlag.ERROR);
		return null;
	}

	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(formato_fecha);
		return sdf.format(cal.getTime());
	}

	private static void readArgs(String arg) {
		try {
			String[] args = arg.split(",");
			for (int i = 0; i < args.length; i++) {
				args[i] = args[i].trim();
				String[] pair = args[i].split("=");
				if (pair[0].equals("epsilon"))
					Properties.__epsilon__ = Double.valueOf(pair[1]);
				if (pair[0].equals("memoria"))
					Properties.__mem_size__ = Integer.valueOf(pair[1]);
				if (pair[0].equals("temporales"))
					Properties.__temps__ = Integer.valueOf(pair[1]);
				if (pair[0].equals("main"))
					Properties.__main__ = pair[1];
				if (pair[0].equals("pusht"))
					Properties.__pusht__ = Boolean.valueOf(pair[1]);

			}
		} catch (Exception e) {
			System.err
					.println("El argumento no es valido. Ejem: epsilon=0.00000000000000001,memoria=4096,"
							+ "temporales=800,main=main(),pusht=false,path=C:\\Users\\julia_000\\Desktop\\MySQL.db");
		}
	}
}
